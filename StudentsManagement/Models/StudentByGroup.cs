﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsManagement.Models
{
    public class StudentByGroup
    {
        public int Id { get; set; }
        public virtual Group Group { get; set; }
        public virtual User User { get; set; }

    }
}