﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentsManagement.Models
{
    public class DisplayStudentViewModel
    {
        public int Id { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> GroupId { get; set; }
        public Nullable<int> CourseId { get; set; }
        public Nullable<int> TeacherId { get; set; }

        public virtual Course Course { get; set; }
        public virtual Group Group { get; set; }
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}