﻿using StudentsManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentsManagement.Controllers
{
    public class LoginController : Controller
    {
        StudentsManagementEntities db = new StudentsManagementEntities();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(User objchk)
        {

            if (ModelState.IsValid)
            {
                using (StudentsManagementEntities db = new StudentsManagementEntities())
                {
                    var obj = db.Users.Where(a => a.Username.Equals(objchk.Username) && a.Password.Equals(objchk.Password)).FirstOrDefault();

                    if (obj != null)
                    {
                        Session["UserId"] = obj.Id.ToString();
                        Session["UserName"] = obj.Username.ToString();
                       
                        if(obj.Role.ToString().Equals("2"))
                        {
                            Session["Admin"] = obj.Role.ToString();
                        }
                        if (obj.Role.ToString().Equals("1"))
                        {
                            Session["Teacher"] = obj.Role.ToString();
                        }
                        return RedirectToAction("Index", "Home");

                    }
                    else
                    {
                        ModelState.AddModelError("", "The Username or password Incorrect");

                    }
                }

            }


            return View(objchk);
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}