﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using StudentsManagement.Models;
using Newtonsoft.Json;
using System.Threading;

namespace StudentsManagement.Controllers
{
    public class AjaxDisplayByGroupController : Controller
    {
        private StudentsManagementEntities db = new StudentsManagementEntities();

        // GET: AjaxDisplayByGroup
        public ActionResult Index()
        {
            var registrations = db.Registrations.Include(r => r.Course).Include(r => r.Group).Include(r => r.User).Include(r => r.User1);
            return View(registrations.ToList());
        }

        [HttpPost]
        public ActionResult FirstAjax(string id)
        {
           
            var data = new { status = "ok", result = id};
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStudentByGroup (string id)

        {
           
         

           var query = db.Registrations.ToList().Where(a => a.GroupId.ToString().Equals(id)).ToString();
            
            for(int i = 0; i < query.Count() ; i++)
            {

            }
            
            List<Registration> listreg = new List<Registration>();
           
            DataTable dt = new DataTable();
            dt.Columns.Add("Id", Type.GetType("System.Int32"));
            dt.Columns.Add("Group", Type.GetType("System.String"));
           
            
            string  data = JsonConvert.SerializeObject(query);
         

           // JSONString = JsonConvert.SerializeObject
          //  DataTable dtPersons = oDAL.GetTable();
        //    List<Dictionary<string, object>> lstPersons = query;
           // var data = DataTableToJSONWithJSONNet(data1);
           

            return Json(data, JsonRequestBehavior.AllowGet);
            //return Json(db.Registrations.ToList().Where(a => a.GroupId.ToString().Equals(id)));
           // return Json(db.Registrations.Where(x => x.GroupId.Equals(id)).ToList(), JsonRequestBehavior.AllowGet);

        }

       
        public JsonResult GetStudentByGroup1(string id)

        {
            
          
                
                List<Registration> studentList = new List<Registration>();
                studentList = db.Registrations.Where(a => a.GroupId.ToString().Equals(id)).ToList();
                for(int i = 0; i < studentList.Count(); i++)
            {
              /*  Registration student = new Registration
                {

                    StudentId = item.StudentId,
                    User = item.User,
                    Group = item.Group

                };
                studentList.Add(student);*/
            }
            
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(studentList, jsonSerializerSettings);
            var count = studentList.Count();
              /*  foreach (var item in studentList)
                {
                    Registration student = new Registration
                    {

                        StudentId = item.StudentId,
                        User = item.User,
                        Group = item.Group

                    };
                    studentList.Add(student);
                   
                   
                }*/
                
                // data = JsonConvert.SerializeObject(studentList);
                //var data1 = studentList.ToArray();
               // var data2 = data1[0].Group;
              

            return Json(count, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetStudentByGroup2(string id)

        {
            List<Registration> studentList = new List<Registration>();
            List<StudentByGroup> studentList1 = new List<StudentByGroup> ();
            studentList = db.Registrations.Where(a => a.GroupId.ToString().Equals(id)).ToList();
           
            foreach (var item in studentList)
            {
                StudentByGroup student = new StudentByGroup
                {

                    User = item.User,
                  
                    Group = item.Group

                };
                studentList1.Add(student);


            }
            

       var jsonSerializerSettings = new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            };
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(studentList1, jsonSerializerSettings);
  


            return Json(data, JsonRequestBehavior.AllowGet);

        }
        public string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }
        /*     public JsonResult GetbyID(int ID)

             {

                 var Employee = empDB.ListAll().Find(x => x.EmployeeID.Equals(ID));

                 return Json(Employee, JsonRequestBehavior.AllowGet);

             }
        */

        // GET: AjaxDisplayByGroup/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registrations.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // GET: AjaxDisplayByGroup/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1");
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName");
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName");
            return View();
        }

        // POST: AjaxDisplayByGroup/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StudentId,GroupId,CourseId,TeacherId")] Registration registration)
        {
            if (ModelState.IsValid)
            {
                db.Registrations.Add(registration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1", registration.CourseId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", registration.GroupId);
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName", registration.StudentId);
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName", registration.TeacherId);
            return View(registration);
        }

        // GET: AjaxDisplayByGroup/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registrations.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1", registration.CourseId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", registration.GroupId);
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName", registration.StudentId);
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName", registration.TeacherId);
            return View(registration);
        }

        // POST: AjaxDisplayByGroup/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StudentId,GroupId,CourseId,TeacherId")] Registration registration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1", registration.CourseId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", registration.GroupId);
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName", registration.StudentId);
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName", registration.TeacherId);
            return View(registration);
        }

        // GET: AjaxDisplayByGroup/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registrations.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // POST: AjaxDisplayByGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Registration registration = db.Registrations.Find(id);
            db.Registrations.Remove(registration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
