﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using StudentsManagement.Models;

namespace StudentsManagement.Controllers
{
    public class RegistrationController : Controller
    {
        private StudentsManagementEntities db = new StudentsManagementEntities();

        // GET: Registration
        public ActionResult Index()
        {
            var registrations = db.Registrations.Include(r => r.Course).Include(r => r.Group).Include(r => r.User).Include(r => r.User1);
            return View(registrations.ToList());
        }

        // GET: Registration/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registrations.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // GET: Registration/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1");
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName");
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName");
            return View();
        }

        // POST: Registration/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StudentId,GroupId,CourseId,TeacherId")] Registration registration)
        {
            if (ModelState.IsValid)
            {
                db.Registrations.Add(registration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1", registration.CourseId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", registration.GroupId);
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName", registration.StudentId);
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName", registration.TeacherId);
            return View(registration);
        }

        // GET: Registration/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registrations.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1", registration.CourseId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", registration.GroupId);
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName", registration.StudentId);
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName", registration.TeacherId);
            return View(registration);
        }

        // POST: Registration/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StudentId,GroupId,CourseId,TeacherId")] Registration registration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "Course1", registration.CourseId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "GroupName", registration.GroupId);
            ViewBag.StudentId = new SelectList(db.Users, "Id", "FirstName", registration.StudentId);
            ViewBag.TeacherId = new SelectList(db.Users, "Id", "FirstName", registration.TeacherId);
            return View(registration);
        }

        // GET: Registration/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registration registration = db.Registrations.Find(id);
            if (registration == null)
            {
                return HttpNotFound();
            }
            return View(registration);
        }

        // POST: Registration/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Registration registration = db.Registrations.Find(id);
            db.Registrations.Remove(registration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
